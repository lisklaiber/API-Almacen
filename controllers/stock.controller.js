const CONN = require('./connection.js');

function GetStock(req, res) {
    CONN('STOCK AS S').leftJoin('PRODUCTO AS P', 'S.producto', '=', 'P.id').leftJoin('TIPO_MEDIDA AS TM', 'P.tipoMedida', '=', 'TM.id')
        .leftJoin('POSICION AS POS', 'S.id', '=', 'POS.stock').leftJoin('COORDENADA AS C', 'POS.coordenada', '=', 'C.id')
        .leftJoin('CODIGO_PROD AS CP', 'P.id', '=', 'CP.producto').select('S.id', 'P.codigo', 'P.nombre', 'P.descripcion', 'P.costo', 'TM.nombre AS medida',
        'TM.prefijo', 'S.cantidad', 'C.x', 'C.y', 'CP.codCliente', 'CP.codProveedor').then(productos => {

            if (!productos.length) {
                res.status(200).send({ resp: [], message: `No hay productos en stock` });
            } else {
                res.status(200).send({ resp: productos });
            }

        }).catch(error => {
            res.status(500).send({ error: `${error}` });
        }
    );
}

module.exports = {
    GetStock
}