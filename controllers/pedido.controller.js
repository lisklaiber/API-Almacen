'use strict'

const CONN = require('./connection.js');

function GetPendientes(req, res) {
    const IDCLIENTE = req.params.idCliente;
    if (!IDCLIENTE){
        CONN('DETALLE_PEDIDO AS DP').leftJoin('CLIENTE AS C', 'DP.cliente', '=', 'C.id').leftJoin('ESTADO AS E', 'DP.estado', '=', 'E.id')
            .whereNot('DP.estado', 4).select('DP.id', 'DP.codigo', 'C.nombre AS cliente', 'DP.fechaPedido', 
            'DP.fechaEntrega', 'DP.descripcion1', 'DP.descripcion2', 'DP.monto', 'E.estado').then( pendientes => {

                if(!pendientes.length){
                    res.status(200).send({ resp: [], message: `No hay pedidos pendientes` });
                } else {
                    res.status(200).send({ resp: pendientes });
                }

            }).catch( error => {
                res.status(500).send({ error: `${error}` });
            }
        );
    } else {
        CONN('DETALLE_PEDIDO AS DP').leftJoin('CLIENTE AS C', 'DP.cliente', '=', 'C.id').leftJoin('ESTADO AS E', 'DP.estado', '=', 'E.id')
            .whereNot('DP.estado', 4).andWhere('DP.cliente', IDCLIENTE).select('DP.id', 'DP.codigo', 'C.nombre AS cliente', 'DP.fechaPedido',
            'DP.fechaEntrega', 'DP.descripcion1', 'DP.descripcion2', 'DP.monto', 'E.estado').then(pendientes => {

                if (!pendientes.length) {
                    res.status(200).send({ resp: [], message: `No hay pedidos pendientes` });
                } else {
                    res.status(200).send({ resp: pendientes });
                }

            }).catch(error => {
                res.status(500).send({ error: `${error}` });
            }
        );
    }
}

function GetProductos(req, res){
    const IDPEDIDO = req.params.idPedido;

    CONN('PEDIDO_PRODUCTO AS PP').leftJoin('PRODUCTO AS P', 'PP.producto', '=', 'P.id').leftJoin('TIPO_MEDIDA AS TM', 'P.tipoMedida', '=', 'TM.id')
        .leftJoin('CODIGO_PROD AS CP', 'P.id', '=', 'CP.producto').where('PP.pedido', IDPEDIDO).select('PP.id', 'P.codigo', 'CP.codCliente', 'P.nombre', 
        'PP.cantidad', 'TM.nombre AS medida', 'TM.prefijo', 'PP.monto').then( productos => {

            res.status(200).send({ resp: productos });

        }).catch(error => {
            res.status(500).send({ error: `${error}` });
        }
    );
}

function GetTiendas(req, res) {
    const IDPEDIDO = req.params.idPedido;

    CONN('PEDIDO_TIENDA AS PT').leftJoin('TIENDA AS T', 'PT.tienda', '=', 'T.id').leftJoin('PRODUCTO AS P', 'PT.producto', '=', 'P.id')
        .leftJoin('TIPO_MEDIDA AS TM', 'P.tipoMedida', '=', 'TM.id').leftJoin('CODIGO_PROD AS CP', 'P.id', '=', 'CP.producto')
        .where('PT.pedido', IDPEDIDO).select('PT.id', 'T.codigo', 'T.nombre', 'P.codigo AS codProd', 'P.nombre AS nomProd', 'TM.nombre AS medida', 
        'TM.prefijo', 'CP.codCliente', 'PT.cantidad').then( tiendas => {

            res.status(200).send({ resp: tiendas });

        }).catch( error => {
            res.status(500).send({ error: `${error}` });
        }
    );
}

module.exports = {
    GetPendientes,
    GetProductos,
    GetTiendas
}