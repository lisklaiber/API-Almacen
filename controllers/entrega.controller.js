const CONN = require('./connection');

function GetPedidos(req, res) {
    CONN('ENTREGA_PEDIDO AS EP').leftJoin('DETALLE_PEDIDO AS DP', 'EP.pedido', '=', 'DP.id').leftJoin('CLIENTE AS C', 'DP.cliente', '=', 'C.id')
        .select('EP.id', 'DP.codigo', 'C.codigo AS codCleinte', 'C.nombre', 'DP.fechaPedido', 'DP.fechaEntrega', 'DP.monto', 'EP.comentario', 
        'EP.fecha').then(entregas => {
            if (!entregas.length) {
                res.status(200).send({ resp: [], message: `No hay entregas realizada` });
            } else {
                res.status(200).send({ resp: entregas });
            }
        }).catch (error => {
            res.status(500).send({ error: `${error}` });
        }
    );
}

module.exports = {
    GetPedidos
}