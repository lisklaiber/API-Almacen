const EXPRESS = require('express');
const ENTREGAcONTROLLER = require('../controllers/entrega.controller.js');

let api = EXPRESS.Router();

api.get('/GetPedidos', ENTREGAcONTROLLER.GetPedidos);

module.exports = api;