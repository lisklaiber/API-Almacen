'use strict'

const EXPRESS = require('express');
const PEDIDOCONTROLLER = require('../controllers/pedido.controller.js');

let api = EXPRESS.Router();

api.get('/GetPendientes/:idCliente?', PEDIDOCONTROLLER.GetPendientes);
api.get('/GetProductos/:idPedido', PEDIDOCONTROLLER.GetProductos);
api.get('/GetTiendas/:idPedido', PEDIDOCONTROLLER.GetTiendas);

module.exports = api;