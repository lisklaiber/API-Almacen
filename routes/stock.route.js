const EXPRESS = require('express');
const STOCKCONTROLLER = require('../controllers/stock.controller.js');

let api = EXPRESS.Router();

api.get('/GetStock', STOCKCONTROLLER.GetStock);

module.exports = api;