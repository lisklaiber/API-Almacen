'use strict'

let app = require('./app.js');
const port = process.env.PORT || 3678;

app.listen(port, () => {
    console.log(`API REST funcionando en http://localhost:${port} `);
});