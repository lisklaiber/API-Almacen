'use strict'

let express = require('express');
let bodyParser = require('body-parser');

let app = express();
const APIPEDIDO = require('./routes/pedido.route.js');
const APISTOCK = require('./routes/stock.route.js');
const APIENTREGA = require('./routes/entrega.route.js');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Acces-Control-Request-Method, enctype');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTION, PUT, DELETE');

    next();
});

app.use('/api/pedido', APIPEDIDO);
app.use('/api/stock', APISTOCK);
app.use('/api/entrega', APIENTREGA);

module.exports = app;