'use strict'

class EntregaPedido {
    constructor(pedido, comentario, fecha) {
        this[`${pedido.tipo}`] = pedido.id;
        this.comentario = comentario;
        this.fecha = fecha;
    }
}

module.exports = EntregaPedido;