'use strict'

class PedidoProducto {
    constructor(pedido, producto, cantidad, monto) {
        this[`${ pedido.tipo }`] = pedido.id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.monto = monto;
    }
}

module.exports = PedidoProducto;