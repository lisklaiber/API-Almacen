'use strict'

const CatProd = require('./CatProd.js');

class ColorProd {
    constructor(codigo, nombre, descripcion, color){
        super(codigo, nombre, descripcion);
        this.color = color;
    }
}

module.exports = ColorProd;