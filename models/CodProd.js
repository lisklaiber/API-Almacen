'use strict'
class CodProd {
    constructor(producto, codProveedor, codCliente){
        this.producto = producto;
        this.codProveedor = codProveedor;
        this.codCliente = codCliente;
    }
}

module.exports = CodProd;