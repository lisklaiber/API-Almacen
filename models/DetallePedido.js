'use strict'

class DetallePedido {
    constructor(codigo, empresa, fechaPedido, fechaEntrega, descripcion1, descripcion2, monto, estado){
        this.codigo = codigo;
        this[`${ empresa.tipo }`] = empresa.id;
        this.fechaPedido = fechaPedido;
        this.fechaEntrega = fechaEntrega;
        this.descripcion1 = descripcion1;
        this.descripcion2 = descripcion2;
        this.monto = monto;
        this.estado = estado;
    }
}

module.exports = DetallePedido;