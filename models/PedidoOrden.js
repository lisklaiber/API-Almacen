'use strict'

class PedidoOrden {
    constructor(pedido, orden) {
        this.pedido = pedido;
        this.orden = orden;
    }
}

module.exports = PedidoOrden;