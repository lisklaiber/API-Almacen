class TipoMedida {
    constructor(nombre, prefijo){
        this.nombre = nombre;
        this.prefijo = prefijo;
    }
}

module.exports = TipoMedida;