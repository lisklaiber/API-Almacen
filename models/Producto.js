'use strict'

const CATPROD = require('./CatProd.js');

class Producto extends CATPROD {
    constructor(codigo, nombre, descripcion, costo, tipoMedida, tipo, categoria, color) {
        super(codigo, nombre, descripcion);
        this.costo = costo;
        this.tipoMedida = tipoMedida;
        this.tipo = tipo;
        this.categoria = categoria;
        this.color = color;
    }
}

module.exports = Producto;