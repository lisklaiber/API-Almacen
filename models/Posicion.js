'use strict'

class Posiscion {
    constructor(coordenada, stock){
        this.coordenada = coordenada;
        this.stock = stock;
    }
}

module.exports = Posiscion;