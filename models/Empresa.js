' use strict'

const CATPROD = require('./CatProd.js');

class Empresa extends CATPROD {
    constructor(codigo, nombre, descripcion, rfc, logo, color, direccion) {
        super(codigo, nombre, descripcion);
        this.rfc = rfc;
        this.logo = logo;
        this.color = color;
        this.direccion = direccion;
    }
}

module.exports = Empresa;