'use strict'
class Stock {
    constructor(producto, cantidad){
        this.producto = producto;
        this.cantidad = cantidad;
    }
}

module.exports = Stock;