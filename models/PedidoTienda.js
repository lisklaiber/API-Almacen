'use strict'

class PedidoTienda {
    constructor(pedido, tienda, producto, cantidad) {
        this.pedido = pedido;
        this.tienda = tienda;
        this.producto = producto;
        this.cantidad = cantidad;
    }
}

module.exports = PedidoTienda;