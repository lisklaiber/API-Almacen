'use strict'

const CATPROD = require('./CatProd');

class Tienda extends CATPROD{
    constructor(codigo, nombre, descripcion, cliente) {
        super(codigo, nombre, descripcion);
        this.cliente = cliente;
    }
}

module.exports = Tienda;